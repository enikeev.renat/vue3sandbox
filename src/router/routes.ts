import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
	{
		path: '/',
		name: 'home',
		component: () => import('@/pages/home/index.vue').then((m) => m.default),
	},
	{
		path: '/about',
		name: 'about',
		component: () => import('@/pages/about/index.vue').then((m) => m.default),
	},
]

export default routes
