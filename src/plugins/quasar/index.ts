import 'quasar/dist/quasar.sass'
import '@quasar/extras/material-icons/material-icons.css'

import { Quasar } from 'quasar'

export const quasar = Quasar;

export const quasarOptions = {
	config: {},
	plugins: {
	}
}
