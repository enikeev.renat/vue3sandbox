import instance from './instance'
import MockAdapter from 'axios-mock-adapter'


export default function () {

	const mock = new MockAdapter(instance, { onNoMatch: "throwException" })

	mock.onGet('/api/currentuser').reply(() => {
		return new Promise(function(resolve, reject) {
			setTimeout(function() {
				resolve([
					200,
					{
						firstName: 'Garold',
						lastName: 'Smith',
						age: '48',
					},
				]);
			}, 2000);
		});

		/*return [
			200,
			{
				name: 'Gerold',
				age: '48',
			},
		]*/
	})

}
