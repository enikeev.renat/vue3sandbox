import axios from '@/plugins/api/instance'

export default {
	user: {
		getCurrentUser(){
			return axios.get('/api/currentuser').then(r => r.data)
		},
	}
}
