import axios from 'axios'

/**
 *	Here you can set config defaults for axios (see https://github.com/axios/axios#config-defaults)
 */

export default axios
