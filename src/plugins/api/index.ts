import axios from './instance'
import client from './client'

export {
	axios,
	client,
}
