import { createApp } from 'vue'
import App from '@/App.vue'
import router from '@/router/index'
import store from '@/store/index'
import {quasar, quasarOptions} from '@/plugins/quasar/index'

;(async () => {
	if (import.meta.env.DEV) {
		let fake = await import('@/plugins/api/fake')
		fake.default()
	}

	const app = createApp(App)
	app.use(quasar, quasarOptions)
	app.use(router)
	app.use(store)
	app.mount('#app')
})()