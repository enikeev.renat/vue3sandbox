import {client} from '@/plugins/api'
import { Store, Commit } from 'vuex'

interface User {
	name:string,
	age:number,
}

interface State {
	currentUser:User
}

const user = {
	namespaced: true,

	state() {
		return {
			currentUser: null,
		}
	},

	mutations: {
		SET_CURRENT_USER(state:State, user:User) {
			state.currentUser = user;
		}
	},

	actions: {
		async getCurrentUser({commit}: { commit: Commit }) {
			const user = await client.user.getCurrentUser();
			commit('SET_CURRENT_USER', user)
		}
	},

	getters: {
		currentUser: (state: State) => state.currentUser
	}
}

export default user
