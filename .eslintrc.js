module.exports = {
	root: true,
	env: {
		es2021: true,
		browser: true,
		node: true,
	},
	extends: [
		'plugin:vue/vue3-essential',
		'eslint:recommended',
		'@vue/typescript/recommended',
		'@vue/prettier',
		'@vue/prettier/@typescript-eslint',
	],
	parserOptions: {
		ecmaVersion: 2020,
	},
	plugins: [],
	rules: {
		'no-unused-vars': 'warn',
		'vue/no-unused-vars': 'warn',
		'prefer-const': 'warn',
		'prettier/prettier': 'warn',
	},
}
