import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import eslintPlugin from 'vite-plugin-eslint'

import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [vue(), eslintPlugin()],
	css: {
		preprocessorOptions: {
			scss: {
				/**
				 * todo
				 * Проверить подключение стилей.
				 */
				additionalData: `
					@import ${resolve(__dirname, 'src', 'styles', '_variables.scss')};
					@import ${resolve(__dirname, 'src', 'styles', '_mixins.scss')};
				`
			},
		},
	},
	resolve: {
		alias: {
			'@': resolve(__dirname, 'src'),
		},
		extensions: ['.mjs', '.js', '.json', '.jsx', '.ts', '.vue'],
	},
})
